import tkinter as tk
import tkinter.messagebox
import tkinter.filedialog
import sys


def MsgBox(params):
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        root.withdraw()
        # tk.Tk().wm_attributes('-topmost', 1)
        # tk.Tk().withdraw()
        message = params['message']
        title = params['title']
        button = params['button']
        icon = params['icon']
        msg = tkinter.messagebox.showinfo(title=title, message=message, icon=icon, type=button)
        return msg
    except Exception as e:
        raise e


def InputBox(params):
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        message = params['message']
        title = params['title']
        default = params['default']
        # onlyNum = params['number']
        root.title(title)
        string = tk.StringVar()
        string.set(default)
        label = tk.Label(root, text=message, padx=20, pady=10)
        label.grid(row=0, column=0, sticky=tk.NW, columnspan=2)
        text = tk.Entry(root, textvariable=string, width=55)
        text.grid(row=2, column=0, sticky=tk.NW, padx=20, columnspan=2, pady=5)
        bt1 = tk.Button(root, text="确定", width=10, command=root.destroy)
        bt2 = tk.Button(root, text="取消", width=10, command=lambda: cancel(string, root))
        bt1.grid(row=3, column=0, sticky=tk.SW, padx=50, pady=5, columnspan=2)
        bt2.grid(row=3, column=1, sticky=tk.SE, padx=50, pady=5, columnspan=2)
        root.mainloop()

        return string.get()
    except Exception as e:
        raise e


def cancel(string, root):
    string.set("")
    root.destroy()


def FileBox(params):
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        path = params['path']
        title = params['title']
        filterFile = params['filter']
        multiple = False if params['multiple'] == 'no' else True
        root.withdraw()
        if not multiple:
            temp = list()
            temp.append(str(tk.filedialog.askopenfilename(initialdir=path, title=title, filetypes=filterFile, multiple=multiple)))
            return temp[0]
        else:
            temp = list(tk.filedialog.askopenfilename(initialdir=path, title=title, filetypes=filterFile, multiple=multiple))
            return temp
    except Exception as e:
        raise e


def SaveFileBox(params):
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        path = params['path']
        title = params['title']
        filterFile = params['filter']
        root.withdraw()
        return tk.filedialog.asksaveasfile(initialdir=path, title=title, filetypes=filterFile,
                                           defaultextension='txt')
    except Exception as e:
        raise e


def NotifyBox(params):
    try:
        root = tk.Tk()
        root.wm_attributes('-topmost', 1)
        root.withdraw()
        message = params['message']
        title = params['title']
        icon = params['icon']
        return eval("tkinter.messagebox.show" + icon + "(title=title, message=message, icon=icon, type='ok')")
    except Exception as e:
        raise e
