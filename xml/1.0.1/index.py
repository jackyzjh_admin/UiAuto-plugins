import json
import xmltodict


def xml_to_dict(params):
    result = None
    if params['source_type'] == 'Text':
        if params['xml_content'] is None or params['xml_content'] == '':
            raise Exception('缺少参数：xml内容')
        result = xmltodict.parse(params['xml_content'])
        result = json.loads(json.dumps(result))
    elif params['source_type'] == 'File':
        if params['xml_file'] is None or params['xml_file'] == '':
            raise Exception('缺少参数：xml文件')
        with open(params['xml_file'], 'r', encoding='UTF-8') as xml_file:
            result = xmltodict.parse(xml_file.read())
        result = json.loads(json.dumps(result))
    else:
        raise Exception('该数据来源暂未支持')
    
    return result


def format_data(data):
    result = {}
    keys = data.keys()
    for key in keys:
        if isinstance(data[key], str):
            result[key] = data[key]
        elif key == 'userdata':
            if isinstance(data[key], list):
                for child in data[key]:
                    result[child['@name']] = (child['#text'] if '#text' in child.keys() else '')
            else:
                result[data[key]['@name']] = data[key]['#text']
        elif key == 'cell':
            result[key] = []
            if isinstance(data[key], list):
                for child in data[key]:
                    result[key].append((child['#text'] if '#text' in child.keys() else ''))
            else:
                result[key].append((data['#text'] if '#text' in data.keys() else ''))
        elif isinstance(data[key], dict):
            result[key] = format_data(data[key])
        
    return result

if __name__ == "__main__":
    # with open("Z:\\workspace\\legion\\code\\uiauto-plugins\\xml\\test.xml", "r", encoding="UTF-8") as xml_file:
    #     result = xmltodict.parse(xml_file.read())
    # print(json.loads(json.dumps(result)))

    data = {
        'rows': {
            'userdata': {
                '@name': 'totalnumber',
                '#text': '1'
            },
            'row': {
                '@id': '0',
                'userdata': [{
                    '@name': 'AHDM',
                    '#text': '255420191002029671'
                }, {
                    '@name': 'AH',
                    '#text': '（2019）粤0104执28459号'
                }, {
                    '@name': 'XLA'
                }, {
                    '@name': 'AJZT',
                    '#text': '500'
                }, {
                    '@name': 'AJLX',
                    '#text': '5101'
                }, {
                    '@name': 'XTAJLX',
                    '#text': '51'
                }, {
                    '@name': 'SAR',
                    '#text': '440104luyq'
                }, {
                    '@name': 'CBR',
                    '#text': '440104liangjiaj'
                }, {
                    '@name': 'SJY'
                }, {
                    '@name': 'LABM',
                    '#text': '44010499'
                }, {
                    '@name': 'CBBM1',
                    '#text': '44010461'
                }, {
                    '@name': 'PCZT',
                    '#text': '0'
                }, {
                    '@name': 'SFZZ',
                    '#text': '0'
                }],
                'cell': [{
                    '@class': 'xmlZC',
                    '#text': '1'
                }, {
                    '@class': 'xmlZC',
                    '#text': '执行'
                }, {
                    '@title': ' ',
                    '@valign': 'middle',
                    '#text': 'http://146.0.250.209:81/zxxt/images/blank.gif'
                }, {
                    '@style': 'color:#000000',
                    '#text': '79'
                }, {
                    '@class': 'xmlZC',
                    '#text': 'http://146.0.250.209:81/zxxt/images/ajgl_view.gif^浏览^javascript:viewAjxx("255420191002029671");^_self'
                }, {
                    '@class': 'xmlZC',
                    '@title': '点击打开案件办理页面',
                    '#text': '（2019）粤0104执28459号^javascript:doLc("0","255420191002029671","5101","500","51", "", "0", "")^_self'
                }, {
                    '@class': 'xmlZC',
                    '#text': '申请执行人:广州市越秀区人民法院; 被执行人:刘建勇'
                }, {
                    '@class': 'xmlZC',
                    '#text': '一审受理费'
                }, {
                    '@class': 'xmlZC',
                    '#text': '梁嘉俊'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC',
                    '#text': '黄一凡'
                }, {
                    '@class': 'xmlZC',
                    '#text': '19-07-15'
                }, {
                    '@class': 'xmlZC',
                    '#text': '327.90'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC',
                    '@title': ''
                }, {
                    '@class': 'xmlZC',
                    '@title': '视频'
                }, {
                    '@class': 'xmlZC',
                    '#text': '184'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC',
                    '#text': '0.00'
                }, {
                    '@class': 'xmlZC',
                    '#text': '执行局'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC'
                }, {
                    '@class': 'xmlZC',
                    '#text': '（2018）粤0104民初5884号'
                }]
            }
        }
    }

    

    print(format_data(data))