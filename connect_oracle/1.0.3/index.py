from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime
import json
from pprint import pprint
import os
import shutil
from traceback import print_exc

"""
need:cx-Oracle==6.0, 32bit oracle11g-client(oci.dll, oraocci11.dll, oraociei11.dll), 32bit python.exe
"""
# 防止中文打印乱码
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'


def execute_oracle_sql(params):
    current_dir = os.path.split(os.path.realpath(__file__))[0]
    if os.path.exists(params['uiauto_config']['client_dir'] + '\\oci.dll') is False:
        shutil.copy(current_dir + '\\oci.dll', params['uiauto_config']['client_dir'] + '\\oci.dll')

    if os.path.exists(params['uiauto_config']['client_dir'] + '\\oraocci11.dll') is False:
        shutil.copy(current_dir + '\\oraocci11.dll', params['uiauto_config']['client_dir'] + '\\oraocci11.dll')

    if os.path.exists(params['uiauto_config']['client_dir'] + '\\oraociei11.dll') is False:
        shutil.copy(current_dir + '\\oraociei11.dll', params['uiauto_config']['client_dir'] + '\\oraociei11.dll')
        

    connect_info = "oracle://{}:{}@{}/{}?utf8".format(params['username'], params['password'], params['host'],
                                                      params['database'])
    engine = create_engine(connect_info, echo=False)  # mysql连接语句
    db_session = sessionmaker(bind=engine)
    session = db_session()

    try:
        if params['sql'].lower().startswith('select'):
            res = session.execute(params['sql']).fetchall()
            res_list = []
            for one in res:
                one_row = []
                for clu in one:
                    if type(clu) is datetime.datetime:
                        clu = str(clu)
                    one_row.append(clu)
                res_list.append(one_row)
            pprint(json.dumps(res_list, ensure_ascii=False))
            session.close()
            return res_list
        else:
            session.execute(params['sql'])
            session.commit()
            session.close()
            return True
        # with open('C:\logs\\123.txt', 'w') as f:
        #     f.write(str(res_list))
    except Exception as ex:
        session.close()
        print_exc()
        return '执行出错：{}'.format(ex)
