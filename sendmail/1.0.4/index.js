const path = require('path');
const _ = require('lodash');
const XLSX = require('xlsx');
const nodemailer = require('nodemailer');

exports.sendMailFn = function(params) {
    const promise = new Promise((resolve, reject) => {
        var sendContent = params.sendContent;
        switch (params.method) {
            case 'text':
                break;
            case 'html':
                break;
            case 'table':
                sendContent = organizeHtmlTable(sendContent);
            default:
                break;
        }

        // set default addresser

        let transporter_config = {
            host: params.senderMailServer,
            port: params.senderMailPort,
            secure: params.senderMailPort == '465' ? true : false,
            auth: {
                user: params.senderMailbox,
                pass: params.senderMailPassword
            }
        }

        let mailOptions = {
            from: params.senderMailbox,
            to: params.receiverMailbox,
            cc: params.copy_send,
            subject: params.subject,
            html: sendContent,
        };

        let transporter = nodemailer.createTransport(transporter_config);

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.error(error);
                reject(error);
            } else {
                console.log('success');
                resolve(info);
            }
        });
    })
    return promise;
}


var organizeHtmlTable = (data) => {
    var header = _.keys(_.extend.apply(this, data));
    var output = XLSX.utils.json_to_sheet(data, {
        header: header,
        skipHeader: false
    })
    var outputPos = Object.keys(output);
    var ref = 'A1:' + outputPos[outputPos.length - header.length - 2];
    var wb = {
        SheetNames: ['mySheet'],
        Sheets: {
            'mySheet': Object.assign({}, output, {
                '!ref': ref
            })
        }
    };
    var filePath = ".out.xlsx";
    XLSX.writeFile(wb, filePath);

    var workbook = XLSX.readFile(filePath);

    return XLSX.utils.sheet_to_html(workbook.Sheets[workbook.SheetNames[0]]) +
        '<style lang="scss" scoped>\n' +
        'td {border-right:1px solid #eee;border-bottom:1px solid #eee;}' +
        'table {border-left:1px solid #eee;border-top:1px solid #eee;}' +
        '</style>';
}