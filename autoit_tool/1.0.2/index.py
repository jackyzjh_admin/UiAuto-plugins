import win32api
import autoit
import traceback


# 装饰器用来捕获异常
def easy_try_except(fun):
    def add_cap(*args):
        try:
            result = fun(*args)
            return result
        except:
            traceback.print_exc()
            return

    return add_cap


@easy_try_except
def start_client(params):
    win32api.ShellExecute(0, 'open', params['exe_path'], '', '', 1)
    return


@easy_try_except
def control_click(params):
    if params['title_or_class'] == 'class':
        class_ = '[CLASS:{}]'.format(params['class'])
        autoit.control_click(class_, params['class_name_nn'])
    else:
        autoit.control_click(params['title'], params['class_name_nn'])
    return


@easy_try_except
def control_set_text(params):
    if params['title_or_class'] == 'class':
        class_ = '[CLASS:{}]'.format(params['class'])
    else:
        class_ = params['title']
    if params['clear']:
        autoit.control_set_text(class_, params['class_name_nn'], '')
    autoit.control_set_text(class_, params['class_name_nn'], params['input_text'])
    return


@easy_try_except
def win_activate(params):
    if params['title_or_class'] == 'class':
        class_ = '[CLASS:{}]'.format(params['class'])
    else:
        class_ = params['title']
    autoit.win_activate(class_)
    return


@easy_try_except
def win_set_on_top(params):
    if params['title_or_class'] == 'class':
        class_ = '[CLASS:{}]'.format(params['class'])
    else:
        class_ = params['title']
    autoit.win_set_on_top(class_)
    return

@easy_try_except
def win_get_text(params):
    if params['title_or_class'] == 'class':
        class_ = '[CLASS:{}]'.format(params['class'])
    else:
        class_ = params['title']
    return autoit.win_get_text(class_)



@easy_try_except
def close_client(params):
    if params['title_or_class'] == 'class':
        class_ = '[CLASS:{}]'.format(params['class'])
    else:
        class_ = params['title']
    autoit.win_close(class_)
    return
